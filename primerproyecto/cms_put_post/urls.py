from django.urls import path

from . import views

urlpatterns = [
    path('<str:recurso>/', views.get_resources),
]