from django.db import models

# Create your models here.
# dict de clave=recurso y valor=contenido de la pagina html


class Contenido(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.TextField()
